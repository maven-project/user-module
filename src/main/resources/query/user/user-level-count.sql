select count(*)
from users u
         inner join users logged on logged.uc = :userCode
where u.user_type = 'web'
  and u.rc = :roleCode
  and u.status = :status
  and if(:userOnline, true, u.is_online = 0)
  and case
          when :roleCode = 'sub-account' then u.parent_id = logged.id
          when logged.user_type = 'system' then true
          when logged.rc = 'super-senior' then u.ssc = logged.uc
          when logged.rc = 'senior' then u.sc = logged.uc
          when logged.rc = 'master' then u.mc = logged.uc
          when logged.rc = 'agent' then u.ac = logged.uc
          else u.uc = logged.uc end

  and if(:keyword = 'all', true, u.un like concat(:keyword, '%') or
                                 u.nn like concat(:keyword, '%') or
                                 u.uc like concat(:keyword, '%'))