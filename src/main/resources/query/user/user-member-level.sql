SELECT u.`uc` userCode
FROM users u
WHERE u.rc = 'member'
  AND (u.`ssc` = :userCode
    OR u.`sc` = :userCode
    OR u.`mc` = :userCode
    OR u.`ac` = :userCode
    OR u.`uc` = :userCode)