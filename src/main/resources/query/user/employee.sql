select u.`uc`           userCode,
       u.un             username,
       u.nn             nickname,
       u.`rc`           roleCode,

       u.language_code  languageCode,
       u.currency_code  currencyCode,
       u.country_code   countryCode,
       u.last_login_web lastLongin,
       u.status
from users u
where u.user_type = 'system'
  and u.rc <> 'super-admin'

  and if(:keyword = 'all', true, u.un like concat(:keyword, '%') or
                                 u.nn like concat(:keyword, '%') or
                                 u.uc like concat(:keyword, '%'))
group by u.un
order by u.un;