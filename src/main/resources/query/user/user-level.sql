select u.`uc`                                                                      userCode,
       u.un                                                                        username,
       u.nn                                                                        nickname,
       u.`rc`                                                                      roleCode,

       ifnull(tb.balance, 0)                       as                              balance,
       sum(if(tr.type = 'deposit', tr.amount, 0))  as                              deposit,
       sum(if(tr.type = 'withdraw', tr.amount, 0)) as                              withdraw,

       u.ssc                                       AS                              superSeniorCode,
       u.sc                                        AS                              seniorCode,
       u.mc                                        AS                              masterCode,
       u.ac                                        AS                              agentCode,
       u.language_code                                                             languageCode,
       u.currency_code                                                             currencyCode,
       u.country_code                                                              countryCode,
       u.platform_type                                                             platformType,
       u.`user_type`                                                               userType,
       u.is_locked_betting                                                         isLockedBetting,
       u.status,
       if(u.last_login_app > u.last_login_web, u.last_login_app, u.last_login_web) lastLogin,
       u.`created_at`                                                              createdAt
from users u
         left join `user_balance` tb ON tb.uc = u.uc
         left join user_transactions tr on tr.uc = u.uc
         left join users logged on logged.uc = :userCode
where u.user_type = 'web'
  and u.rc = :roleCode
  and u.status = :status
  and if(:userOnline, true, u.is_online = 0)
  and case
          when :roleCode = 'sub-account' then u.parent_id = logged.id
          when logged.user_type = 'system' then true
          when logged.rc = 'super-senior' then u.ssc = logged.uc
          when logged.rc = 'senior' then u.sc = logged.uc
          when logged.rc = 'master' then u.mc = logged.uc
          when logged.rc = 'agent' then u.ac = logged.uc
          else u.uc = logged.uc end

  and if(:keyword = 'all', true, u.un like concat(:keyword, '%') or
                                 u.nn like concat(:keyword, '%') or
                                 u.uc like concat(:keyword, '%'))
group by u.un
order by u.un;