SELECT r.id,
       r.code,
       ifnull(r.name, r.code)                                as name,
       GROUP_CONCAT(DISTINCT (ifnull(rhp.permission_id, 0))) AS permission
FROM roles r
         LEFT JOIN role_has_permissions rhp ON r.id = rhp.role_id
WHERE r.code <> 'sub-account'
  AND r.code <> 'super-admin'
  AND r.guard_name = :userType
  AND if(:userType = 'web', r.code in (:roleCode), true)
  AND if(:keyword = 'all', true, r.code like concat(:keyword, '%') or r.name like concat(:keyword, '%'))
GROUP BY r.id
ORDER BY r.id