package com.module.user.service;

import com.module.user.constant.GeneralConstant;
import com.module.user.constant.MessageConstant;
import com.module.user.data.entity.PermissionEntity;
import com.module.user.data.entity.RoleEntity;
import com.module.user.data.entity.RoleHasPermissionEntity;
import com.module.user.data.model.request.permission.PermissionListRQ;
import com.module.user.data.model.request.permission.UpdatePermissionRQ;
import com.module.user.data.model.request.role.CreateRoleRQ;
import com.module.user.data.model.request.role.RoleListRQ;
import com.module.user.data.model.request.role.UpdateRoleRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.data.model.response.permission.PermissionRS;
import com.module.user.data.model.response.role.RoleRS;
import com.module.user.data.repo.MerchantRP;
import com.module.user.data.repo.PermissionRP;
import com.module.user.data.repo.RoleHasPermissionRP;
import com.module.user.data.repo.RoleRP;
import com.module.user.exception.httpstatus.BadRequestException;
import com.module.user.utils.auth.JwtTokenUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.module.user.constant.MessageConstant.*;

@Service
@RequiredArgsConstructor
public class RoleService extends BaseService {

    private final MerchantRP merchantRP;
    private final RoleRP roleRP;
    private final RoleHasPermissionRP rolePermissionRP;
    private final PermissionRP permissionRP;
    private final HttpServletRequest request;
    private final JwtTokenUtility jwtTokenUtility;

    public void createDefault(Long merchantId) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setMerchantId(merchantId);
        roleEntity.setCode(GeneralConstant.ADMIN_ROLE);
        roleEntity.setCreatedBy(null);
        roleEntity.setStatus(Boolean.TRUE);
        roleRP.save(roleEntity);
    }

    public StructureRS create(CreateRoleRQ createRoleRQ) {

        if (!merchantRP.existsById(createRoleRQ.getMerchantId()))
            return responseBodyWithBadRequest(MERCHANT_DOES_NOT_EXISTED, MERCHANT_DOES_NOT_EXISTED_KEY);

        String codeRole = createRoleRQ.getCode().replaceAll("\\s+","").toLowerCase();

        RoleEntity checkRole = roleRP.checkRole(createRoleRQ.getMerchantId(), codeRole);
        if (checkRole != null)
            return responseBodyWithBadRequest(ROLE_EXISTED, ROLE_EXISTED_KEY);

        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setCode(codeRole);
        roleEntity.setMerchantId(createRoleRQ.getMerchantId());
        roleEntity.setCreatedBy(jwtTokenUtility.getUserToken().getId());
        roleEntity.setStatus(createRoleRQ.getStatus());
        roleRP.save(roleEntity);
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);
    }

    public StructureRS roles(Long merchantId) {
        RoleListRQ roleListRQ = new RoleListRQ(request);
        Page<RoleEntity> roleEntities = roleRP.roles(merchantId, PageRequest.of(roleListRQ.getPage(), roleListRQ.getSize()));
        List<RoleRS> roleRSList = new ArrayList<>();
        roleEntities.forEach(item -> {
            RoleRS roleRS = new RoleRS();
            BeanUtils.copyProperties(item, roleRS);
            roleRSList.add(roleRS);
        });
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, roleRSList);
    }

    public StructureRS updateRole(UpdateRoleRQ updateRoleRQ) {

        RoleEntity roleEntity = roleRP.role(updateRoleRQ.getRoleId());
        if (roleEntity != null) {
            roleEntity.setStatus(updateRoleRQ.getStatus());
            roleRP.save(roleEntity);
            return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);
        }

        throw new BadRequestException("Role does not exist");

    }

    public StructureRS permissions() {
        PermissionListRQ permissionListRQ = new PermissionListRQ(request);
        RoleEntity roleEntity = roleRP.checkRole(permissionListRQ.getMerchantId(), permissionListRQ.getRoleCode());
        if (roleEntity == null)
            return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);

        List<PermissionEntity> permissionEntities = permissionRP.permissions();
        List<RoleHasPermissionEntity> rolePermissionEntities = rolePermissionRP.permissions(roleEntity.getId());

        List<PermissionRS> permissionRSList = new ArrayList<>();
        for (PermissionEntity permission : permissionEntities) {
            PermissionRS permissionRS = new PermissionRS();
            for (RoleHasPermissionEntity hasPermission : rolePermissionEntities) {
                if (isRoleHasPermission(permission, hasPermission, roleEntity.getId()))
                    permissionRS.setStatus(Boolean.TRUE);
            }

            /**
             * Set permissionId & roleId
             */
            permissionRS.setPermissionId(permission.getId().intValue());
            permissionRS.setRoleId(roleEntity.getId().intValue());
            BeanUtils.copyProperties(permission, permissionRS);
            permissionRSList.add(permissionRS);
        }

        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, permissionRSList);

    }

    public StructureRS updatePermission(UpdatePermissionRQ updatePermissionRQ) {

        if (!roleRP.existsById(updatePermissionRQ.getRoleId()))
            throw new BadRequestException("Role does not exist");

        if (!permissionRP.existsById(updatePermissionRQ.getPermissionId()))
            throw new BadRequestException("Permission does not exist");

        RoleHasPermissionEntity hasPermission = rolePermissionRP.hasPermission(updatePermissionRQ.getRoleId(), updatePermissionRQ.getPermissionId());

        if (hasPermission != null) {
            hasPermission.setStatus(updatePermissionRQ.getStatus());
            rolePermissionRP.save(hasPermission);
        } else {
            RoleHasPermissionEntity createPermission = new RoleHasPermissionEntity();
            createPermission.setPermissionId(updatePermissionRQ.getPermissionId());
            createPermission.setRoleId(updatePermissionRQ.getRoleId());
            createPermission.setStatus(updatePermissionRQ.getStatus());
            rolePermissionRP.save(createPermission);
        }

        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);

    }

    /**
     * check is role has permission
     * @param permission PermissionEntity
     * @param hasPermission RoleHasPermissionEntity
     * @param roleId Integer
     * @return boolean
     */
    private boolean isRoleHasPermission(PermissionEntity permission, RoleHasPermissionEntity hasPermission, Long roleId) {
        return  (permission.getId().intValue() == hasPermission.getPermissionId() &&
                Objects.equals(hasPermission.getRoleId(), roleId) &&
                hasPermission.getStatus());
    }
}
