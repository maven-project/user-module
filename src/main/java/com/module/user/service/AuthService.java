package com.module.user.service;

import com.module.user.config.AuthConfiguration;
import com.module.user.constant.MessageConstant;
import com.module.user.constant.UserConstant;
import com.module.user.constant.UserStatusEnum;
import com.module.user.data.entity.ContactsEntity;
import com.module.user.data.entity.UserEntity;
import com.module.user.data.model.request.auth.ForgotPasswordRQ;
import com.module.user.data.model.request.auth.LoginRQ;
import com.module.user.data.model.request.auth.RegisterRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.data.model.response.account.TokenRS;
import com.module.user.data.model.response.account.UserDetailRS;
import com.module.user.data.model.response.account.UserRS;
import com.module.user.data.repo.ContactRP;
import com.module.user.data.repo.UserRP;
import com.module.user.exception.httpstatus.BadRequestException;
import com.module.user.utils.GeneralUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
@RequiredArgsConstructor
public class AuthService extends BaseService {

    private final UserRP userRP;
    private final ContactRP contactRP;
    private final DeviceService deviceSV;
    private final AuthConfiguration authConfiguration;
    private final HttpServletRequest request;
    private final GeneralUtility generalUtility;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public StructureRS register(HttpHeaders httpHeaders, RegisterRQ registerRQ) {
        UserEntity userEntity = new UserEntity();
        userEntity.setCode(generalUtility.userUniqueCode());
        userEntity.setUsername(registerRQ.getUsername());
        userEntity.setEmailPhone(registerRQ.getEmailPhone());
        userEntity.setFirstName(registerRQ.getFirstName());
        userEntity.setLastName(registerRQ.getLastName());
        userEntity.setPassword(passwordEncoder.encode(registerRQ.getPassword()));
        userEntity.setLastLogin(new Date());
        userEntity.setStatus(UserStatusEnum.ACTIVATE);
        UserEntity created = userRP.save(userEntity);

        /*
         * Contact
         */
        ContactsEntity contactsEntity = new ContactsEntity();
        contactsEntity.setParentId(created.getId().intValue());
        contactRP.save(contactsEntity);

        /*
         * Add device
         */
        deviceSV.trackDevice(request, created);

        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);
    }

    public StructureRS login(HttpHeaders httpHeaders, LoginRQ loginRQ) {
        String credential = authConfiguration.oauth2Credential();

        if (!credential.equals(httpHeaders.getFirst(HttpHeaders.AUTHORIZATION)))
            throw new BadRequestException(MessageConstant.INCORRECT_CREDENTIAL);

        UserEntity user = userRP.getUserByUsername(loginRQ.getUsernameEmailPhone());

        if (user == null)
            throw new BadRequestException(MessageConstant.INCORRECT_USERNAME_OR_PASSWORD);

        if (!passwordEncoder.matches(loginRQ.getPassword(), user.getPassword()))
            throw new BadRequestException(MessageConstant.INCORRECT_USERNAME_OR_PASSWORD);

        if (UserStatusEnum.DEACTIVATE.equals(user.getStatus()))
            throw new BadRequestException(MessageConstant.DEACTIVATE);

        TokenRS tokenRS = authConfiguration.getCredential(loginRQ, credential);

        UserRS userRS = new UserRS();
        BeanUtils.copyProperties(user, userRS);
        UserDetailRS userDetailRS = new UserDetailRS();
        userDetailRS.setUser(userRS);
        userDetailRS.setToken(tokenRS.getAccess_token());

        user.setLastLogin(new Date());
        userRP.save(user);

        /*
         * Track device
         */
        deviceSV.trackDevice(request, user);

        return responseBodyWithSuccessMessage(userDetailRS);

    }

    public StructureRS forgotPassword(HttpHeaders httpHeaders, ForgotPasswordRQ forgotPasswordRQ) {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, "Implement when integrate firebase");
    }

    public StructureRS healthStatus() {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, "Okay");
    }

    public StructureRS version() {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, "v1.0.1");
    }

}
