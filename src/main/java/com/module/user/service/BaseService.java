package com.module.user.service;

import com.module.user.constant.MessageConstant;
import com.module.user.data.model.response.PagingRS;
import com.module.user.data.model.response.StructureRS;
import org.springframework.http.HttpStatus;

/**
 * @author Sombath
 * create at 23/10/22 5:57 PM
 */
public class BaseService {


    
    public StructureRS response(Object data) {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, data);
    }

    
    public StructureRS responseBodyWithSuccessMessage() {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);
    }

    
    public StructureRS responseBodyWithSuccessMessage(Object data) {
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, data);
    }

    public StructureRS responseBodyWithBadRequest(String message, String messageTr)
    {
        return responseBody(HttpStatus.BAD_REQUEST, message, messageTr, null, new PagingRS());
    }

    
    public StructureRS responseBody(HttpStatus status) {
        return responseBody(status, null, null);
    }

    
    public StructureRS responseBody(HttpStatus status, String message) {
        return responseBody(status, message, null);
    }

    public StructureRS responseBody(HttpStatus status, String message, Object data)
    {
        return responseBody(status, message, data, new PagingRS());
    }

    public StructureRS responseBody(HttpStatus status, String message, Object data, PagingRS paging)
    {
        StructureRS structureRS = new StructureRS();
        structureRS.setStatus(status.value());
        structureRS.setMessage(message);
        structureRS.setData(data);
        structureRS.setPaging(paging);

        return structureRS;
    }

    public StructureRS responseBody(HttpStatus status, String message, String messageTr, Object data, PagingRS paging)
    {
        StructureRS structureRS = new StructureRS();
        structureRS.setStatus(status.value());
        structureRS.setMessage(message);
        structureRS.setMessageKey(messageTr);
        structureRS.setData(data);
        structureRS.setPaging(paging);

        return structureRS;
    }
    
}
