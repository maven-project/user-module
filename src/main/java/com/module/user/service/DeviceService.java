package com.module.user.service;

import com.module.user.constant.MessageConstant;
import com.module.user.data.entity.DevicesEntity;
import com.module.user.data.repo.DevicesRP;
import com.module.user.data.entity.UserEntity;
import com.module.user.data.model.request.device.DeviceListRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.data.model.response.device.DeviceRS;
import com.module.user.utils.auth.JwtTokenUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceService extends BaseService {

    private final DevicesRP devicesRP;
    private final HttpServletRequest request;
    private final JwtTokenUtility jwtTokenUtility;

    public StructureRS list() {
        DeviceListRQ deviceListRQ = new DeviceListRQ(request);
        Page<DevicesEntity> devicesEntities = devicesRP.lists(jwtTokenUtility.getUserToken().getId().intValue(), PageRequest.of(deviceListRQ.getPage(), deviceListRQ.getSize()));
        List<DeviceRS> deviceRSList = new ArrayList<>();
        devicesEntities.forEach(item -> {
            DeviceRS deviceRS = new DeviceRS();
            BeanUtils.copyProperties(item, deviceRS);
            deviceRSList.add(deviceRS);
        });
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, deviceRSList);
    }

    public void trackDevice(HttpServletRequest request, UserEntity userEntity) {
        DevicesEntity checkDevice = devicesRP.checkDevice(userEntity.getId().intValue(), request.getHeader("X-DeviceId"));
        if (checkDevice == null) {
            DevicesEntity devicesEntity = new DevicesEntity();
            devicesEntity.setUserId(userEntity.getId().intValue());
            devicesEntity.setDeviceId(request.getHeader("X-DeviceId"));
            devicesEntity.setDeviceName(request.getHeader("X-DeviceName"));
            devicesEntity.setDeviceModel(request.getHeader("X-DeviceModel"));
            devicesRP.save(devicesEntity);
        }
    }

    @Transactional
    public StructureRS remove(Integer id) {
        devicesRP.removeDevice(id);
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, null);
    }

}
