package com.module.user.service;

import com.module.user.constant.GeneralConstant;
import com.module.user.constant.MessageConstant;
import com.module.user.constant.UserStatusEnum;
import com.module.user.data.entity.UserEntity;
import com.module.user.data.model.request.account.LanguageRQ;
import com.module.user.data.model.request.account.LogoutRQ;
import com.module.user.data.model.request.account.ResetRQ;
import com.module.user.data.model.request.account.UpdateRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.data.model.response.account.UserDetailRS;
import com.module.user.data.model.response.account.UserRS;
import com.module.user.data.repo.ContactRP;
import com.module.user.data.repo.UserRP;
import com.module.user.exception.httpstatus.BadRequestException;
import com.module.user.utils.GeneralUtility;
import com.module.user.utils.UploadFileUtility;
import com.module.user.utils.auth.JwtTokenUtility;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Service
@RequiredArgsConstructor
public class AccountService extends BaseService {

    private final UserRP userRP;
    private final ContactRP contactRP;
    private final HttpServletRequest request;
    private final JwtTokenUtility jwtTokenUtility;
    private final GeneralUtility generalUtility;
    private final UploadFileUtility uploadFileUtility;

    public StructureRS profile() {
        UserEntity user = userRP.getUserByUsername(jwtTokenUtility.getUserToken().getUsername());
        UserRS userRS = new UserRS();
        BeanUtils.copyProperties(user, userRS);

        if (userRS.getThumbnail() != null) {
            userRS.setThumbnail(generalUtility.imgBaseUrl(GeneralConstant.DR_USER_THUMBNAIL_URL, userRS.getThumbnail()));
        }

        UserDetailRS userDetailRS = new UserDetailRS();
        userDetailRS.setUser(userRS);
        return responseBody(HttpStatus.OK, MessageConstant.SUCCESSFULLY, userDetailRS);
    }

    public void update(UpdateRQ updateRQ, MultipartFile file) {
        UserEntity userEntity = userRP.getUserByUsername(jwtTokenUtility.getUserToken().getUsername());
        System.out.println(userEntity);
        if (userEntity == null)
            throw new BadRequestException(MessageConstant.ACCOUNT_NOT_EXISTED, MessageConstant.ACCOUNT_NOT_EXISTED_KEY, null);

        if (file != null) {
            userEntity.setThumbnail(uploadFileUtility.uploadFile(file, GeneralConstant.DR_USER_THUMBNAIL_URL));
        }
        if (updateRQ.getFirstName() != null) {
            userEntity.setFirstName(updateRQ.getFirstName());
        }
        if (updateRQ.getLastName() != null) {
            userEntity.setLastName(updateRQ.getLastName());
        }
        if (updateRQ.getDob() != null) {
            userEntity.setDob(generalUtility.parseDateFromString(updateRQ.getDob()));
        }
        if (updateRQ.getGender() != null) {
            userEntity.setGender(updateRQ.getGender());
        }
        if (updateRQ.getCurrencyCode() != null) {
            userEntity.setCountryCode(updateRQ.getCurrencyCode());
        }
        if (updateRQ.getLanguageCode() != null) {
            userEntity.setLanguageCode(updateRQ.getLanguageCode());
        }
        if (updateRQ.getBloodType() != null) {
            userEntity.setBloodType(updateRQ.getBloodType());
        }
        if (updateRQ.getPosition() != null) {
            userEntity.setPosition(updateRQ.getPosition());
        }
        if (updateRQ.getMarital() != null) {
            userEntity.setMarital(updateRQ.getMarital());
        }
        if (updateRQ.getIdCard() != null) {
            userEntity.setIdCard(updateRQ.getIdCard());
        }
        if (updateRQ.getStatus() != null) {
            userEntity.setStatus(UserStatusEnum.valueOf(updateRQ.getStatus()));
        }
        userRP.save(userEntity);

    }

    public void language(LanguageRQ languageRQ) {
        UserEntity userEntity = userRP.getUserByUsername(jwtTokenUtility.getUserToken().getUsername());
        userEntity.setLanguageCode(languageRQ.getLanguageCode().toUpperCase());
        userRP.save(userEntity);
    }

    public void resetPassword(ResetRQ resetRQ) {
        UserEntity userEntity = userRP.getUserByUsername(jwtTokenUtility.getUserToken().getUsername());
        if (userEntity == null)
            throw new BadRequestException(MessageConstant.ACCOUNT_NOT_EXISTED, MessageConstant.ACCOUNT_NOT_EXISTED_KEY, null);

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        if (!bCryptPasswordEncoder.matches(resetRQ.getCurrentPassword(), userEntity.getPassword()))
            throw new BadRequestException(MessageConstant.INCORRECT_ONLY_PASSWORD, MessageConstant.INCORRECT_ONLY_PASSWORD_KEY, null);

        userEntity.setPassword(bCryptPasswordEncoder.encode(resetRQ.getNewPassword()));
        generalUtility.setLastLogin(userEntity);

        userRP.save(userEntity);
    }


    public StructureRS logout(LogoutRQ loginRQ) {
        return response(null);
    }

}
