package com.module.user.utils.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class JwtTokenUtility {

    private final HttpServletRequest request;
    private final GenericConversionService genericConversionService;

    private  <T> T getClaim(Map<String, ?> tokenData, String claimKey, Class<T> type) {
        try {
            return genericConversionService.convert(tokenData.get(claimKey), type);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private Map<String, ?> parseToken() {
        try {
            JsonParser parser = JsonParserFactory.getJsonParser();
            String authorization = request.getHeader("Authorization");
            return parser.parseMap(JwtHelper.decode(authorization.split(" ")[1]).getClaims());
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public <T> T getClaim(String claimKey, Class<T> type, String authorization) {
        try {
            JsonParser parser = JsonParserFactory.getJsonParser();
            Map<String, ?> tokenData = parser.parseMap(JwtHelper.decode(authorization.split(" ")[1]).getClaims());
            return genericConversionService.convert(tokenData.get(claimKey), type);
        } catch (Exception ex) {
            return null;
        }
    }

    public UserToken getUserToken() {

        // avoiding parse token many time
        Map<String, ?> tokenData = this.parseToken();

        UserToken userToken = new UserToken();
        userToken.setId(this.getClaim(tokenData, "id", Long.class));
        userToken.setCode(this.getClaim(tokenData, "code", String.class));
        userToken.setUsername(this.getClaim(tokenData, "username", String.class));
        userToken.setEmailPhone(this.getClaim(tokenData, "emailPhone", String.class));
        userToken.setFirstName(this.getClaim(tokenData, "firstName", String.class));
        userToken.setLastName(this.getClaim(tokenData, "lastName", String.class));
        userToken.setThumbnail(this.getClaim(tokenData, "thumbnail", String.class));
        userToken.setCurrencyCode(this.getClaim(tokenData, "currencyCode", String.class));
        userToken.setUserType(this.getClaim(tokenData, "userType", String.class));
        String _permissions = this.getClaim(tokenData,"permissions", String.class);
        if (_permissions != null) {
            userToken.setPermissions(Stream.of(_permissions.split(",")) .collect(Collectors.toList()));
        }
        return userToken;
    }

}