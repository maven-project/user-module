package com.module.user.utils.auth;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserToken {
    private Long id;
    private String code;
    private String username;
    private String emailPhone;
    private String firstName;
    private String lastName;
    private String thumbnail;
    private Date dob;
    private String gender;
    private String userType;
    private String currencyCode;
    private String languageCode;
    private String bloodType;
    private String position;
    private String marital;
    private String idCard;
    private Date lastLogin;
    private String status;
    private List<String> permissions;
}