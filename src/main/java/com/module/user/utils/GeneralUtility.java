package com.module.user.utils;

import com.module.user.data.entity.UserEntity;
import com.module.user.data.repo.MerchantRP;
import com.module.user.data.repo.UserRP;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class GeneralUtility {

    private final UserRP userRP;
    private final MerchantRP merchantRP;
    private final Environment environment;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static String getUniqueCode(String lastCode) {

        Calendar calendar = Calendar.getInstance();

        String newYear = Integer.toString(calendar.get(Calendar.YEAR)).substring(2, 4);
        if (lastCode == null) {
            return "00000001" + newYear;
        }

        String currentYear = lastCode.substring(lastCode.length() - 2).substring(0, 2);
        if (!currentYear.equals(newYear)) {
            return "00000001" + newYear;
        }

        String numIncr = lastCode.substring(0, 1);
        String num7Digit = lastCode.substring(1, 8);

        if (num7Digit.equals("9999999")) {

            num7Digit = "0000001";
            if (numIncr.equals("9")) {

                return "A" + num7Digit + currentYear;

            } else if (!NumberUtils.isCreatable(numIncr)) {

                return (char) (numIncr.charAt(0) + 1) + num7Digit + currentYear;

            } else {

                return (NumberUtils.toInt(numIncr) + 1) + num7Digit + currentYear;
            }

        } else {

            String increase = Integer.toString(NumberUtils.toInt(num7Digit) + 1);
            return numIncr + "0000000".substring(increase.length()) + increase + currentYear;

        }

    }

    public String userUniqueCode() {
        return getUniqueCode(userRP.getUser() != null ? userRP.getUser().getCode() : null);
    }

    public String merchantUniqueCode() {
        return getUniqueCode(merchantRP.getLastMerchant() != null ? merchantRP.getLastMerchant().getCode() : null);
    }

    public void setLastLogin(UserEntity userEntity) {
        userEntity.setLastLogin(new Date());
    }

    public Date parseDateFromString(String date) {
        if (date != null) {
            try {
                return dateFormat.parse(date);
            } catch (ParseException parseException) {
                System.out.println("GeneralUtility.parseDateFromString parseException date = " + date);
                parseException.printStackTrace();
            }

        }
        return new Date();
    }

    public String imgBaseUrl(String dirUrl, String thumbnailUrl) {
       return environment.getProperty("spring.AWS_CONFIG.URL.UPLOAD_URL") + dirUrl + thumbnailUrl;
    }

}
