package com.module.user.utils;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.module.user.constant.MessageConstant;
import com.module.user.exception.httpstatus.BadRequestException;
import com.module.user.exception.httpstatus.InternalServerError;
import lombok.RequiredArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@RequiredArgsConstructor
public class UploadFileUtility {

    private final AmazonS3 s3client;
    private final Environment environment;


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Implement upload image
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @Param url
     * @Param fileName
     * @Param largPath
     * @Param smallPath
     * @Return String
     */
    public String uploadFile(MultipartFile multipartFile, String path) {

        // set folder by date
        Calendar date = new GregorianCalendar();
        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONTH);
        String dateFolder = year+"-"+month;

        String fileName = generateFileName(null);
        Optional<String> imageEx;

        try {
            File file = convertMultiPartToFile(multipartFile);
            String fileNames = multipartFile.getOriginalFilename();

            imageEx = getExtensionByStringHandling(fileNames);
            limitFileType(imageEx.get(), new String[]{"PNG", "JPG", "JPEG"});

            s3bucket(fileName + "." + imageEx.get(), file, path+dateFolder);
            file.delete();

        } catch (Exception e) {
            throw new InternalServerError("Internal Server Error", null);
        }

        return dateFolder+"/"+fileName + "." + imageEx.get();

    }

    private File convertMultiPartToFile(MultipartFile file) throws IOException {
        File convFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    private void limitFileType(String ext, String[] fileType) {
        boolean b = false;
        for (int i = 0; i < fileType.length; i++) {
            if (fileType[i].equals(ext.toUpperCase())) {
                b = true;
            }
        }
        if (!b) {
            throw new BadRequestException(MessageConstant.FILE_NOT_ALLOW, MessageConstant.FILE_NOT_ALLOW_KEY, null);
        }
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Generate unique name for file
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @Return String
     */
    public String generateFileName(String ext)
    {
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String name = UUID.randomUUID().toString().replace("-", "").substring(0, 10) + timeStamp;

        if (ext != null) {
            name = name + "." + ext;
        }
        return name;
    }


    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Get extension image
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @Param filename
     */
    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Upload file to amazon
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @Param filename
     * @Param file
     */
    private void s3bucket(String fileName, File file, String path) {
        try {
            s3client.putObject(new PutObjectRequest(environment.getProperty("spring.AWS_CONFIG.BUCKET")+path, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
        } catch (AmazonServiceException e) {
            e.getMessage();
        }
    }

}
