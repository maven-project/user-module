package com.module.user.exception;

import com.module.user.data.model.response.StructureRS;
import com.module.user.exception.httpstatus.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InternalServerError.class)
    public ResponseEntity<StructureRS> globalExceptionHandler(Exception ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(BadSqlGrammarException.class)
    public ResponseEntity<StructureRS> badSqlGrammarExceptionHandler(Exception ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.INTERNAL_SERVER_ERROR,
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<StructureRS> userNameNotFoundExceptionHandler(UsernameNotFoundException ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.BAD_REQUEST,
                ex.getMessage()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<StructureRS> badRequestExceptionHandler(BadRequestException ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.BAD_REQUEST,
                ex.getMessage()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<StructureRS> forbiddenExceptionHandler(ForbiddenException ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.FORBIDDEN,
                ex.getMessage()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(UnauthorizedException.class)
    public ResponseEntity<StructureRS> unauthorizedExceptionHandler(UnauthorizedException ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.UNAUTHORIZED,
                ex.getMessage()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<StructureRS> notFoundExceptionHandler(NotFoundException ex) {
        StructureRS structureRS = new StructureRS(
                HttpStatus.NOT_FOUND,
                ex.getMessage()
        );
        ex.printStackTrace();
        return new ResponseEntity<>(structureRS, HttpStatus.BAD_REQUEST);
    }

}
