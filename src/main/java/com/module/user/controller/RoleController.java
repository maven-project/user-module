package com.module.user.controller;

import com.module.user.data.model.request.permission.UpdatePermissionRQ;
import com.module.user.data.model.request.role.CreateRoleRQ;
import com.module.user.data.model.request.role.UpdateRoleRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.service.RoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/v1.0.0/role")
@RequiredArgsConstructor
public class RoleController extends BaseController {

    private final RoleService roleSV;

    @GetMapping("/{merchantId}")
    public ResponseEntity<StructureRS> roles(@PathVariable("merchantId") Long merchantId) {
        return response(roleSV.roles(merchantId));
    }

    @PostMapping()
    public ResponseEntity<StructureRS> create(@Valid @RequestBody CreateRoleRQ createRoleRQ) {
        return response(roleSV.create(createRoleRQ));
    }

    @PutMapping()
    public ResponseEntity<StructureRS> updateRole(@Valid @RequestBody UpdateRoleRQ updateRoleRQ) {
        return response(roleSV.updateRole(updateRoleRQ));
    }

    @GetMapping("permission")
    public ResponseEntity<StructureRS> permissions() {
        return response(roleSV.permissions());
    }

    @PostMapping("permission")
    public ResponseEntity<StructureRS> updatePermission(@Valid @RequestBody UpdatePermissionRQ updatePermissionRQ) {
        return response(roleSV.updatePermission(updatePermissionRQ));
    }

}
