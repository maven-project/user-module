package com.module.user.controller;

import com.module.user.data.model.request.auth.ForgotPasswordRQ;
import com.module.user.data.model.request.auth.LoginRQ;
import com.module.user.data.model.request.auth.RegisterRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/v1.0.0/auth")
@RequiredArgsConstructor
public class AuthController extends BaseController {

    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<StructureRS> register(@RequestHeader HttpHeaders httpHeaders, @Validated @RequestBody RegisterRQ registerRQ) {
        return response(authService.register(httpHeaders, registerRQ));
    }

    @PostMapping("/login")
    public ResponseEntity<StructureRS> login(@RequestHeader HttpHeaders httpHeaders, @Validated @RequestBody LoginRQ loginRQ) {
        return response(authService.login(httpHeaders, loginRQ));
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<StructureRS> forgotPassword(@RequestHeader HttpHeaders httpHeaders, @Validated @RequestBody ForgotPasswordRQ forgotPasswordRQ) {
        return response(authService.forgotPassword(httpHeaders, forgotPasswordRQ));
    }

    @GetMapping("/health-status")
    public ResponseEntity<StructureRS> healthStatus() {
        return response(authService.healthStatus());
    }

    @GetMapping("/version")
    public ResponseEntity<StructureRS> version() {
        return response(authService.version());
    }

}
