package com.module.user.controller;

import com.module.user.data.model.response.StructureRS;
import com.module.user.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/api/v1.0.0/device")
@RequiredArgsConstructor
public class DeviceController extends BaseController {

    private final DeviceService deviceSV;

    @GetMapping()
    public ResponseEntity<StructureRS> list() {
        return response(deviceSV.list());
    }

    @DeleteMapping("{id}")
    public ResponseEntity<StructureRS> remove(@PathVariable("id") Integer id) {
        return response(deviceSV.remove(id));
    }

}
