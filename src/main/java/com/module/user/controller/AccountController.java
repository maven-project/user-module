package com.module.user.controller;

import com.module.user.data.model.request.account.LanguageRQ;
import com.module.user.data.model.request.account.LogoutRQ;
import com.module.user.data.model.request.account.ResetRQ;
import com.module.user.data.model.request.account.UpdateRQ;
import com.module.user.data.model.response.StructureRS;
import com.module.user.service.AccountService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/v1.0.0/account")
@RequiredArgsConstructor
public class AccountController extends BaseController {

    private final AccountService accountSV;

    @GetMapping()
    public ResponseEntity<StructureRS> profile() {
        return response(accountSV.profile());
    }

    @PutMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<StructureRS> update(@Valid @ModelAttribute UpdateRQ updateRQ, @RequestParam(value = "thumbnail", required = false) MultipartFile file) {
        accountSV.update(updateRQ, file);
        return response(new StructureRS());
    }

    @PostMapping("/language")
    public ResponseEntity<StructureRS> language(@Valid @RequestBody LanguageRQ languageRQ) {
        accountSV.language(languageRQ);
        return response(new StructureRS());
    }

    @PostMapping("/logout")
    public ResponseEntity<StructureRS> logout(@Validated @RequestBody LogoutRQ loginRQ) {
        return response(accountSV.logout(loginRQ));
    }

    @PostMapping("/reset-password")
    public ResponseEntity<StructureRS> resetPassword(@Valid @RequestBody ResetRQ resetRQ) {
        accountSV.resetPassword(resetRQ);
        return response(new StructureRS());
    }

}
