package com.module.user.security;

import com.module.user.config.UserPrincipal;
import com.module.user.data.entity.UserEntity;
import com.module.user.data.repo.UserRP;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class CustomUserDetails implements UserDetailsService {

    private final UserRP userRP;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {

        UserEntity user = userRP.getUserByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("Invalid Username or password");
        }

        return UserPrincipal.build(user);

    }

}
