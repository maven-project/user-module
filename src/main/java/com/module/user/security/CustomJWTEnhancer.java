package com.module.user.security;

import com.module.user.config.UserPrincipal;
import com.module.user.data.entity.UserEntity;
import com.module.user.data.repo.UserRP;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class CustomJWTEnhancer implements TokenEnhancer  {

    private final UserRP userRP;

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

        Map<String, Object> additionalInfo = new HashMap<>();

        var userPrincipal = (UserPrincipal) authentication.getPrincipal();
        UserEntity userEntity = userRP.getUserByUsername(userPrincipal.getUsername());

        additionalInfo.put("id", userEntity.getId());
        additionalInfo.put("username", userEntity.getUsername());
        additionalInfo.put("code", userEntity.getCode());
        additionalInfo.put("issued", new Date());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }

}
