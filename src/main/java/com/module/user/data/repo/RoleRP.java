package com.module.user.data.repo;

import com.module.user.data.entity.RoleEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * @author Sombath
 * create at 23/10/22 6:36 AM
 */


public interface RoleRP extends JpaRepository<RoleEntity, Long> {

    @Query(value = "SELECT * FROM roles WHERE merchant_id = :merchantId ORDER BY id DESC", nativeQuery = true)
    Page<RoleEntity> roles(Long merchantId, Pageable pageable);


    @Query(value = "SELECT * FROM roles WHERE merchant_id = :merchantId AND code = :code LIMIT 1", nativeQuery = true)
    RoleEntity checkRole(Long merchantId, String code);

    @Query(value = "SELECT * FROM roles WHERE id = :id LIMIT 1", nativeQuery = true)
    RoleEntity role(Long id);

}
