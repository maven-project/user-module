package com.module.user.data.repo;

import com.module.user.data.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * @author Sombath
 * create at 23/10/22 6:36 AM
 */

public interface UserRP extends JpaRepository<UserEntity, Long> {

    @Query(value = "SELECT * FROM users WHERE username = ?1 OR email_phone = ?1 ORDER BY id DESC LIMIT 1", nativeQuery = true)
    UserEntity getUserByUsername(String username);

    @Query(value = "SELECT * FROM users ORDER BY id DESC LIMIT 1", nativeQuery = true)
    UserEntity getUser();
}
