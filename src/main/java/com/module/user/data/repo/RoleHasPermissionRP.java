package com.module.user.data.repo;

import com.module.user.data.entity.RoleHasPermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Sombath
 * create at 23/10/22 6:39 AM
 */

public interface RoleHasPermissionRP extends JpaRepository<RoleHasPermissionEntity, Long> {

    @Query(value = "SELECT * FROM role_has_permissions WHERE role_id = :roleId ORDER BY id DESC", nativeQuery = true)
    List<RoleHasPermissionEntity> permissions(Long roleId);

    @Query(value = "SELECT * FROM role_has_permissions WHERE role_id = :roleId AND permission_id = :permissionId ORDER BY id DESC", nativeQuery = true)
    RoleHasPermissionEntity hasPermission(Long roleId, Long permissionId);
}
