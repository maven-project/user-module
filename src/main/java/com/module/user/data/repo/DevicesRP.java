package com.module.user.data.repo;

import com.module.user.data.entity.DevicesEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface DevicesRP extends JpaRepository<DevicesEntity, Long> {

    @Query(value = "SELECT * FROM devices WHERE user_id = ?1 AND device_id = ?2 LIMIT 1", nativeQuery = true)
    DevicesEntity checkDevice(Integer parentId, String deviceId);

    @Query(value = "SELECT * FROM devices WHERE user_id = ?1 ORDER BY id DESC ", nativeQuery = true)
    Page<DevicesEntity> lists(Integer parentId, Pageable pageable);

    @Modifying
    @Query(value = "DELETE FROM devices WHERE id = ?1", nativeQuery = true)
    void removeDevice(Integer id);

}
