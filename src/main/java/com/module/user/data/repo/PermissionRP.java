package com.module.user.data.repo;

import com.module.user.data.entity.PermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author Sombath
 * create at 23/10/22 6:32 AM
 */

public interface PermissionRP extends JpaRepository<PermissionEntity, Long> {


    @Query(value = "SELECT * FROM permissions ORDER BY id DESC ", nativeQuery = true)
    List<PermissionEntity> permissions();

    @Query(value = "SELECT * FROM permissions WHERE id = :id LIMIT 1", nativeQuery = true)
    PermissionEntity permissionById(Long id);

}
