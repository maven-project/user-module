package com.module.user.data.repo;

import com.module.user.data.entity.ContactsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRP extends JpaRepository<ContactsEntity, Long> {


}
