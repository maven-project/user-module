package com.module.user.data.repo;

import com.module.user.data.entity.MerchantsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRP extends JpaRepository<MerchantsEntity, Long> {

    @Query(value = "SELECT * FROM merchants ORDER BY id DESC LIMIT 1", nativeQuery = true)
    MerchantsEntity getLastMerchant();

    @Query(value = "SELECT * FROM merchants ORDER BY id DESC", nativeQuery = true)
    Page<MerchantsEntity> lists(Pageable pageable);

    @Query(value = "SELECT * FROM merchants WHERE id = ?1 ORDER BY id DESC LIMIT 1", nativeQuery = true)
    MerchantsEntity merchantById(Integer merchantId);

}
