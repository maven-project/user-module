package com.module.user.data.model.request.auth;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Data
public class RegisterRQ {

    @NotNull(message = "Please provide a username")
    @NotEmpty(message = "Please provide a username")
    private String username;

    @NotNull(message = "Please provide a email phone")
    @NotEmpty(message = "Please provide a email phone")
    private String emailPhone;

    @NotNull(message = "Please provide a first name")
    @NotEmpty(message = "Please provide a first name")
    private String firstName;

    @NotNull(message = "Please provide a last name")
    @NotEmpty(message = "Please provide a last name")
    private String lastName;

    @NotNull(message = "Please provide a password")
    @NotEmpty(message = "Please provide a password")
    private String password;

    @NotNull(message = "Please provide a confirm password")
    @NotEmpty(message = "Please provide a confirm password")
    private String confirmPassword;

}