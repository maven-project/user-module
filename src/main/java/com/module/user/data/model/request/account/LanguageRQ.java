package com.module.user.data.model.request.account;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Data
public class LanguageRQ {

    @NotNull(message = "Please provide a language code")
    @NotEmpty(message = "Please provide a language code")
    private String languageCode;

}
