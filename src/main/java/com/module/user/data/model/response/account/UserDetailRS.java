package com.module.user.data.model.response.account;

import lombok.Data;

@Data
public class UserDetailRS {
    UserRS user;
    private String token;
}
