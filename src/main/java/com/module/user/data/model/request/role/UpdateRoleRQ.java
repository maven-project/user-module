package com.module.user.data.model.request.role;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class UpdateRoleRQ {

    @NotNull(message = "Please provide a roleId")
    private Long roleId;

    @NotNull(message = "Please provide a status")
    private Boolean status;

}