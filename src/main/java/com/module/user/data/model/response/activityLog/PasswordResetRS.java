package com.module.user.data.model.response.activityLog;

import lombok.Data;

import java.util.Date;

@Data
public class PasswordResetRS {
    private String deviceType;
    private Date resetAt;
    private String ipAddress;
    private String resetBy;
}
