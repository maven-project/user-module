package com.module.user.data.model.dto.user;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class UserLevelTO {
    private String userCode;
    private String username;
    private String nickname;
    private String roleCode;
    private BigDecimal balance;
    private BigDecimal deposit;
    private BigDecimal withdraw;
    private String superSeniorCode;
    private String seniorCode;
    private String masterCode;
    private String agentCode;
    private String languageCode;
    private String currencyCode;
    private String countryCode;
    private String platformType;
    private String userType;
    private Boolean isLockedBetting;
    private String status;
    private Date lastLogin;
    private Date createdAt;
}