package com.module.user.data.model.dto;

import lombok.Data;

/**
 * @author Sombath
 * create at 27/6/22 1:13 PM
 */

@Data
public class RoleTO {
    private Long id;
    private String name;
    private String code;
    private String permission;
}
