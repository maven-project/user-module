package com.module.user.data.model.request.role;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Data
public class CreateRoleRQ {

    @NotNull(message = "Please provide a code")
    @NotEmpty(message = "Please provide a code")
    private String code;

    @NotNull(message = "Please provide a merchantId")
    private Long merchantId;

    @NotNull(message = "Please provide a name")
    private Boolean status;

}