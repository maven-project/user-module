package com.module.user.data.model.dto.user;

import lombok.Data;

/**
 * @author Sombath
 * create at 8/7/22 4:49 PM
 */

@Data
public class UserLevelPriority {

    private String level;
    private String priority;

}
