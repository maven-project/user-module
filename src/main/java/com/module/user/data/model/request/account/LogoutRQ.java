package com.module.user.data.model.request.account;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class LogoutRQ {

    @NotNull(message = "Please provide a token")
    private String deviceToken;

}
