package com.module.user.data.model.response;

import lombok.Data;

@Data
public class PagingRS {
    private Integer page;
    private Integer size;
    private Long totals;


    public PagingRS() {
    }

    public PagingRS(int number, int size, long totalElements) {
        this.page = number;
        this.size = size;
        this.totals = totalElements;
    }
}
