package com.module.user.data.model.response.permission;

import lombok.Data;

@Data
public class PermissionRS {
    private Long id;
    private String name;
    private String module;
    private Integer permissionId;
    private Integer roleId;
    private Boolean status = Boolean.FALSE;
}