package com.module.user.data.model.request.account;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class ResetRQ {

    @NotNull(message = "Please provide a current password")
    private String currentPassword;
    @NotNull(message = "Please provide a new password")
    private String newPassword;
}
