package com.module.user.data.model.dto.user;

/**
 * @author Sombath
 * create at 8/6/22 10:36 PM
 */
public interface UserReferralTO {

    String getUserCode();
    String getUsername();
    String getNickname();
    String getRoleCode();
    String getRoleName();
    String getSuperSeniorCode();
    String getSeniorCode();
    String getMasterCode();
    String getAgentCode();

}
