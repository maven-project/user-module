package com.module.user.data.model.request.device;

import com.module.user.constant.GeneralConstant;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

@Data
public class DeviceListRQ {
    private int page;
    private int size;
    public DeviceListRQ(HttpServletRequest request) {
        this.page = request.getParameter("page") != null && !request.getParameter("page").isEmpty() ? ( Integer.parseInt(request.getParameter("page") ) - 1 ) : 0;
        this.size = request.getParameter("size") != null && !request.getParameter("size").isEmpty() ? Integer.parseInt(request.getParameter("size")) : GeneralConstant.size;
    }
}
