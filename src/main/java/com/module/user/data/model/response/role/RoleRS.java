package com.module.user.data.model.response.role;

import lombok.Data;

import java.util.Date;

@Data
public class RoleRS {
    private Long id;
    private String code;
    private Integer createdBy;
    private Date createdAt;
    private Boolean status = Boolean.FALSE;
}