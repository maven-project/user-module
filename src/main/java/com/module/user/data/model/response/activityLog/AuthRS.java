package com.module.user.data.model.response.activityLog;

import lombok.Data;

import java.util.Date;

@Data
public class AuthRS {
    private String deviceType;
    private Date loginAt;
    private String ipAddress;
}
