package com.module.user.data.model.response.account;

import lombok.Data;

@Data
public class TokenRS {
    private String access_token;
}
