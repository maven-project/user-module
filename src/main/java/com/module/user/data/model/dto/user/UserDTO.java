package com.module.user.data.model.dto.user;

import com.module.user.constant.UserStatusEnum;

import java.util.Date;

/**
 * @author Sombath
 * create at 28/1/22 5:21 PM
 */
public interface UserDTO {
     String getUserCode();
     String getUsername();
     String getNickname();
     String getRoleCode();
     String getRoleName();
     UserStatusEnum getStatus();
     Boolean getIsLocked();
     Date getCreatedAt();
     String getPlatformType();
     String getLotteryType();
     String getCurrencyCode();
     Date getLastLogin();
}
