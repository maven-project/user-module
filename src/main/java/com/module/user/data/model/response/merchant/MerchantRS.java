package com.module.user.data.model.response.merchant;

import com.module.user.constant.MerchantTypeEnum;
import com.module.user.constant.UserStatusEnum;
import lombok.Data;

@Data
public class MerchantRS {
    private Long id;
    private String code;
    private String name;
    private String thumbnail;
    private String description;
    private Integer parentId;
    private MerchantTypeEnum merchantType = MerchantTypeEnum.MERCHANT;
    private UserStatusEnum status = UserStatusEnum.ACTIVATE;
}

