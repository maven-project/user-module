package com.module.user.data.model.response.merchant;

import lombok.Data;

@Data
public class ModuleRS {
    private String moduleName;
    private Boolean status = Boolean.FALSE;
}
