package com.module.user.data.model.dto.user;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @author Sombath
 * create at 15/6/22 8:34 PM
 */
public interface UserProfileTO {

    String getUserCode();
    String getAgentCode();
    String getMasterCode();
    String getSeniorCode();
    String getSuperSeniorCode();
    String getUsername();
    String getNickname();
    String getRoleCode();
    Double getBalance();
    Double getDeposit();
    Double getWithdraw();
    String getCurrencyCode();
    String getLanguageCode();
    String getPlatformType();
    String getStatus();
    Boolean getIsLockedBetting();
    @JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss", timezone = "Asia/Phnom_Penh")
    Date getCreatedAt();

}
