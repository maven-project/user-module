package com.module.user.data.model.request.permission;

import com.module.user.constant.GeneralConstant;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

@Data
public class PermissionListRQ {
    private int page;
    private int size;
    private Long merchantId;
    private String roleCode;
    public PermissionListRQ(HttpServletRequest request) {
        this.page = request.getParameter("page") != null && !request.getParameter("page").isEmpty() ? ( Integer.parseInt(request.getParameter("page") ) - 1 ) : 0;
        this.size = request.getParameter("size") != null && !request.getParameter("size").isEmpty() ? Integer.parseInt(request.getParameter("size")) : GeneralConstant.size;
        this.merchantId = Long.valueOf(request.getParameter("merchantId"));
        this.roleCode = request.getParameter("roleCode");
    }
}