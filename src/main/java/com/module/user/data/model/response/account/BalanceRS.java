package com.module.user.data.model.response.account;

import com.module.user.constant.GeneralConstant;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class BalanceRS {
    private String userCode;
    private BigDecimal balance = BigDecimal.ZERO;
    private BigDecimal pendingLotto5 = BigDecimal.ZERO;
    private BigDecimal pendingLotto6 = BigDecimal.ZERO;
    private BigDecimal pendingLotto12 = BigDecimal.ZERO;
    private String currencyCode = GeneralConstant.KHR;
    private String currencySignal = "៛";

    public BigDecimal getFullPending() {
        return this.pendingLotto5.add(this.pendingLotto6).add(this.pendingLotto12);
    }
}