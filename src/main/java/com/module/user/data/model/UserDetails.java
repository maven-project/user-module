//package com.module.user.data.model;
//
//import com.module.user.data.entity.UserEntity;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.oauth2.jwt.Jwt;
//
//import java.util.Collection;
//import java.util.List;
//import java.util.stream.Collectors;
//
///**
// * @author Sombath
// * create at 26/10/22 8:54 PM
// */
//public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {
//
//    private final Long id;
//    private String platformType;
//    private String username;
//    private String password;
//    private String nickname;
//    private String userCode;
//    private String userRole;
//    private String currencyCode;
//    private String userType;
//    private String superSeniorCode;
//    private String seniorCode;
//    private String masterCode;
//    private String agentCode;
//
//    private Long parentId;
//    private String parentCode;
//    private String parentRole;
//
//    private List<String> permissions;
//
//    public UserDetails(Jwt jwt) {
//        this.id = Long.valueOf(jwt.getSubject());
//    }
//
//    public UserDetails(UserEntity u, UserEntity parent, List<String> permissions) {
//        this.id = u.getId();
//        this.platformType = u.getPlatformType();
//        this.username = u.getUsername();
//        this.password = u.getPassword();
//        this.nickname = u.getNickname();
//        this.userCode = u.getUserCode();
//        this.userRole = u.getRoleCode();
//        this.currencyCode = u.getCurrencyCode();
//        assert u.getUserType() != null;
//        this.userType = u.getUserType().toString();
//        this.superSeniorCode = u.getSuperSeniorCode();
//        this.seniorCode = u.getSeniorCode();
//        this.masterCode = u.getMasterCode();
//        this.agentCode = u.getAgentCode();
//
//        if(parent != null) {
//            this.parentId = parent.getId();
//            this.parentCode = parent.getUserCode();
//            this.parentRole = parent.getRoleCode();
//        }
//
//        this.permissions = permissions;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public String getPlatformType() {
//        return platformType;
//    }
//
//    public String getNickname() {
//        return nickname;
//    }
//
//    public String getUserCode() {
//        return userCode;
//    }
//
//    public String getUserRole() {
//        return userRole;
//    }
//
//    public String getCurrencyCode() {
//        return currencyCode;
//    }
//
//    public String getUserType() {
//        return userType;
//    }
//
//    public String getSuperSeniorCode() {
//        return superSeniorCode;
//    }
//
//    public String getSeniorCode() {
//        return seniorCode;
//    }
//
//    public String getMasterCode() {
//        return masterCode;
//    }
//
//    public String getAgentCode() {
//        return agentCode;
//    }
//
//    public Long getParentId() {
//        return parentId;
//    }
//
//    public String getParentCode() {
//        return parentCode;
//    }
//
//    public String getParentRole() {
//        return parentRole;
//    }
//
//    @Override
//    public Collection<? extends GrantedAuthority> getAuthorities() {
//        return this.permissions.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
//    }
//
//    @Override
//    public String getPassword() {
//        return this.password;
//    }
//
//    @Override
//    public String getUsername() {
//        return this.username;
//    }
//
//    @Override
//    public boolean isAccountNonExpired() {
//        return Boolean.TRUE;
//    }
//
//    @Override
//    public boolean isAccountNonLocked() {
//        return Boolean.TRUE;
//    }
//
//    @Override
//    public boolean isCredentialsNonExpired() {
//        return Boolean.TRUE;
//    }
//
//    @Override
//    public boolean isEnabled() {
//        return Boolean.TRUE;
//    }
//}
