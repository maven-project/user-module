package com.module.user.data.model.response.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.module.user.constant.UserStatusEnum;
import com.module.user.constant.UserTypeEnum;
import lombok.Data;

import java.util.Date;

@Data
public class UserRS {
    private String code;
    private String username;
    private String emailPhone;
    private String firstName;
    private String lastName;
    private String thumbnail;
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Phnom_Penh")
    private Date dob;
    private String gender;
    private UserTypeEnum userType;
    private String currencyCode;
    private String languageCode;
    private String bloodType;
    private String position;
    private String marital;
    private String idCard;
    private Date lastLogin;
    private UserStatusEnum status;
}
