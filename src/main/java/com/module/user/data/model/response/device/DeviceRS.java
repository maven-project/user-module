package com.module.user.data.model.response.device;

import lombok.Data;

import java.util.Date;

@Data
public class DeviceRS {
    private Long id;
    private String deviceId;
    private String deviceName;
    private String deviceModel;
    private Date createdAt;
}

