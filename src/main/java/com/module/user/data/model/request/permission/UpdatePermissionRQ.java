package com.module.user.data.model.request.permission;

import lombok.Data;

import jakarta.validation.constraints.NotNull;

@Data
public class UpdatePermissionRQ {

    @NotNull(message = "Please provide a permissionId")
    private Long permissionId;

    @NotNull(message = "Please provide a roleId")
    private Long roleId;

    @NotNull(message = "Please provide a status")
    private Boolean status;
}
