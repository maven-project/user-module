package com.module.user.data.model.request.role;

import com.module.user.constant.GeneralConstant;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

@Data
public class RoleListRQ {
    private int page;
    private int size;
    public RoleListRQ(HttpServletRequest request) {
        this.page = request.getParameter("page") != null && !request.getParameter("page").isEmpty() ? ( Integer.parseInt(request.getParameter("page") ) - 1 ) : 0;
        this.size = request.getParameter("size") != null && !request.getParameter("size").isEmpty() ? Integer.parseInt(request.getParameter("size")) : GeneralConstant.size;
    }
}