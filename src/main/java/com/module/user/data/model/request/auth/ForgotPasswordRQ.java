package com.module.user.data.model.request.auth;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Data
public class ForgotPasswordRQ {

    @NotNull(message = "Please provide a username, email, phone")
    @NotEmpty(message = "Please provide a username, email, phone")
    private String usernameEmailPhone;

}
