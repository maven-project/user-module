package com.module.user.data.model.request.account;

import lombok.Data;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Data
public class UpdateRQ {
    @NotNull(message = "Please provide a emailPhone")
    @NotEmpty(message = "Please provide a emailPhone")
    private String emailPhone;

    private String firstName;
    private String lastName;
    private String dob;
    private String gender;
    private String currencyCode;
    private String languageCode;
    private String bloodType;
    private String position;
    private String marital;
    private String idCard;
    private String status;
}
