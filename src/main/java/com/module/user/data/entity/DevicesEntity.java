package com.module.user.data.entity;

import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name="devices")
public class DevicesEntity extends BaseEntity {

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "device_id", unique=true)
    private String deviceId;

    @Nullable
    @Column(name = "device_name")
    private String deviceName;

    @Nullable
    @Column(name = "device_model")
    private String deviceModel;

    @Nullable
    @Column(name = "device_token")
    private String deviceToken;

}




