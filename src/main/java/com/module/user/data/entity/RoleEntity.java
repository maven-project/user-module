package com.module.user.data.entity;

import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "roles")
public class RoleEntity extends BaseEntity {

    @Column(name = "code", unique=true)
    private String code;

    @Column(name = "merchant_id")
    private Long merchantId;

    @Nullable
    @Column(name = "created_by")
    private Long createdBy;

    @Nullable
    @Column(name = "status")
    private Boolean status;

}
