package com.module.user.data.entity;


import com.module.user.constant.UserStatusEnum;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="users")
public class UserEntity extends BaseEntity {

    @Column(name = "code", unique=true)
    private String code;

    @Column(name = "username", unique=true)
    private String username;

    @Column(name = "email_phone", unique=true)
    private String emailPhone;

    @Column
    private String password;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "thumbnail")
    private String thumbnail;

    @Nullable
    @Column(name = "dob")
    private Date dob;

    @Nullable
    @Column(name = "gender")
    private String gender;

    @Nullable
    @Column(name = "language_code")
    private String languageCode;

    @Nullable
    @Column(name = "country_code")
    private String countryCode;

    @Nullable
    @Column(name = "blood_type")
    private String bloodType;

    @Nullable
    @Column(name = "position")
    private String position;

    @Nullable
    @Column(name = "marital")
    private String marital;

    @Nullable
    @Column(name = "id_card")
    private String idCard;

    @Nullable
    @Column(name = "change_password")
    private Integer changePassword;

    @Nullable
    @Column(name = "is_locked_screen")
    private Boolean isLockedScreen;

    @Nullable
    @Column(name = "last_login")
    private Date lastLogin;

    @Nullable
    @Column(name = "verified_at")
    private Date verifiedAt;

    @Nullable
    @Enumerated(EnumType.STRING)
    private UserStatusEnum status = UserStatusEnum.ACTIVATE;

}






