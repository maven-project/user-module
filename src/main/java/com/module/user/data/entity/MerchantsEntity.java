package com.module.user.data.entity;

import com.module.user.constant.MerchantTypeEnum;
import com.module.user.constant.UserStatusEnum;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Data
@Entity
@Table(name="merchants")
public class MerchantsEntity extends BaseEntity {

    @Column(name = "code")
    private String code;

    @Nullable
    @Column(name = "name")
    private String name;

    @Nullable
    @Column(name = "thumbnail")
    private String thumbnail;

    @Nullable
    @Column(name = "description")
    private String description;

    @Nullable
    @Column(name = "parent_id")
    private Integer parentId;

    @Nullable
    @Enumerated(EnumType.STRING)
    private MerchantTypeEnum merchantType = MerchantTypeEnum.MERCHANT;

    @Nullable
    @Enumerated(EnumType.STRING)
    private UserStatusEnum status = UserStatusEnum.ACTIVATE;

    @Nullable
    @Column(name = "created_by")
    private Integer createdBy;

}

