package com.module.user.data.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name="merchant_has_module")
public class MerchantHasModuleEntity extends BaseEntity {

    @Column(name = "merchant_id")
    private Integer merchantId;

    @Column(name = "module_name")
    private String moduleName;

    @Column(name = "status")
    private Boolean status;

}

