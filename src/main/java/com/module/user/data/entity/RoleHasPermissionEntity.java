package com.module.user.data.entity;

import com.sun.istack.Nullable;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Sombath
 * create at 23/10/22 6:12 AM
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "role_has_permissions")
public class RoleHasPermissionEntity extends BaseEntity {

    @Column(name = "role_id", nullable = false)
    private Long roleId;

    @Nullable
    @Column(name = "permission_id")
    private Long permissionId;

    @Column(name = "status")
    private Boolean status;

}
