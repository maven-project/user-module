package com.module.user.data.entity;

import com.module.user.constant.UserTypeEnum;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Data
@Entity
@Table(name="contacts")
public class ContactsEntity extends BaseEntity {

    @Column(name = "parent_id", unique=true)
    private Integer parentId;

    @Nullable
    @Column(name = "subject")
    private String subject;

    @Nullable
    @Column(name = "facebook")
    private String facebook;

    @Nullable
    @Column(name = "telegram")
    private String telegram;

    @Nullable
    @Column(name = "youtube")
    private String youtube;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_phone")
    private String firstPhone;

    @Nullable
    @Column(name = "second_phone")
    private String second_phone;

    @Nullable
    @Column(name = "address")
    private String address;

    @Nullable
    @Column(name = "longitude")
    private String longitude;

    @Nullable
    @Column(name = "latitude")
    private String latitude;

    @Nullable
    @Column(name = "is_primary")
    private Boolean isPrimary;

    @Nullable
    @Enumerated(EnumType.STRING)
    private UserTypeEnum userType = UserTypeEnum.U;

}

