package com.module.user.constant;

public enum UserStatusEnum {
    ACTIVATE, DEACTIVATE, SUSPEND;
}