package com.module.user.constant;

/*
 * author: kangto
 * createdAt: 29/12/2021
 * time: 11:08
 */
public class UserConstant {

    public final static String SUSPEND = "SUSPEND";
    public final static String ACTIVATE = "ACTIVATE";
    public final static String DEACTIVATE = "DEACTIVATE";

    public final static String USER_ONLINE_PERMISSION = "list-user-online";

    // User levels
    public final static String CREATE_SUPER_SENIOR = "CREATE_SUPER-SENIOR";
    public final static String SUPER_SENIOR = "SUPER-SENIOR";
    public final static String SUPER_ADMIN = "SUPER-ADMIN";
    public final static String ADMIN = "ADMIN";
    public final static String STAFF = "STAFF";
    public final static String SENIOR = "SENIOR";
    public final static String MASTER = "MASTER";
    public final static String AGENT = "AGENT";
    public final static String MEMBER = "MEMBER";
    public final static String SUB_ACCOUNT = "SUB-ACCOUNT";
    public final static String CREATE_SUB_ACCOUNT = "CREATE_SUB-ACCOUNT";
    public final static String SYSTEM = "SYSTEM";
    public static final String WEB = "WEB";

    public static String getUnderLevel(String filterByLevel) {
        switch (filterByLevel.toUpperCase()) {
            case UserConstant.SUPER_SENIOR:
                return UserConstant.SENIOR;
            case UserConstant.SENIOR:
                return UserConstant.MASTER;
            case UserConstant.MASTER:
                return UserConstant.AGENT;
            default:
                return UserConstant.MEMBER;
        }
    }
}
