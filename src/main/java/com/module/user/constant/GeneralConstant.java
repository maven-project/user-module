package com.module.user.constant;

public class GeneralConstant {

    public final static String KHR = "KHR";
    public final static String KHR_SIGNAL = "៛";
    public final static String THB = "THB";
    public final static String THB_SIGNAL = "฿";
    public final static String ALL = "ALL";
    public final static String NONE = "NONE";
    public final static int TEMP_ORDER_LIVE_DAY = 2;

    public final static String SUSPEND = "SUSPEND";
    public final static String ACTIVATE = "ACTIVATE";
    public final static String DEACTIVATE = "DEACTIVATE";
    public final static String BANNED = "BANNED";

    public final static String ADMIN_ROLE = "admin";
    public final static String MERCHANT = "MERCHANT";

    public final static int size = 50;

    /**
     * S3 Directory
     */
    public final static String DR_USER_THUMBNAIL_URL = "thumbnail/user/";
    public final static String DR_MERCHANT_THUMBNAIL_URL = "thumbnail/merchant/";

}
