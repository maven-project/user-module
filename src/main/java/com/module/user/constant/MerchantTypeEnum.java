package com.module.user.constant;

public enum MerchantTypeEnum {
    SYSTEM, OWNER, MERCHANT;
}