package com.module.user.constant;

public class MessageConstant {

    public final static String INCORRECT_ONLY_PASSWORD = "Incorrect password";
    public final static String INCORRECT_ONLY_PASSWORD_KEY = "incorrect_password";
    public final static String ACCOUNT_NOT_EXISTED = "Account not yet existed!";
    public final static String ACCOUNT_NOT_EXISTED_KEY = "account_not_yet_existed";
    public final static String TOKEN_LOCKED = "Taken have been locked";

    /**
     * HTTP Message
     */
    public final static String SOMETHING_WENT_WRONG = "Something went wrong";
    public final static String SOMETHING_WENT_WRONG_TR = "something_went_wrong";

    public final static String UNAUTHORIZED = "Unauthorized";
    public final static String UNAUTHORIZED_KEY = "unauthorized";
    public final static String DEACTIVATE = "Account have been deactivated";
    public final static String DEACTIVATE_KEY = "deactivated";

    public final static String SUCCESSFULLY = "Successfully";
    public final static String SUCCESSFULLY_KEY = "success";

    public final static String BAD_REQUEST = "Bad request";
    public final static String BAD_REQUEST_KEY = "bad_request";

    public final static String ROLE_EXISTED = "Role Existed";
    public final static String ROLE_EXISTED_KEY = "role_existed";

    public final static String ROLE_NOT_EXISTED = "Role doesn't existed";
    public final static String ROLE_NOT_EXISTED_KEY = "role_not_existed";

    public final static String PERMISSION_NOT_EXISTED = "Permission doesn't existed";
    public final static String PERMISSION_NOT_EXISTED_KEY = "permission_not_existed";

    public final static String MERCHANT_DOES_NOT_EXISTED = "Merchant doesn't exist";
    public final static String MERCHANT_DOES_NOT_EXISTED_KEY = "merchant_does_not_role_exist";

    public final static String DATA_NOT_FOUND = "Data not found!";
    public final static String DATA_NOT_FOUND_KEY = "data_not_found";

    public static final String LOCK_ACCOUNT = "Your account has been locked!";
    public static final String LOCK_ACCOUNT_KEY = "your_account_has_been_locked";

    public static final String FILE_NOT_ALLOW = "File not allow!";
    public static final String FILE_NOT_ALLOW_KEY = "file_not_allow";

    public static final String INCORRECT_CREDENTIAL = "Incorrect_credential";
    public static final String INCORRECT_USERNAME_OR_PASSWORD = "Incorrect username or password";
}
