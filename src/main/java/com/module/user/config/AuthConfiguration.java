package com.module.user.config;

import com.module.user.data.model.request.auth.LoginRQ;
import com.module.user.data.model.response.account.TokenRS;
import lombok.RequiredArgsConstructor;
import org.apache.commons.codec.binary.Base64;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
@RequiredArgsConstructor
public class AuthConfiguration {

    private final HttpServletRequest request;
    private final Environment environment;
    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Create basic auth
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @param loginRQ
     * @param credential
     * @return
     */
    public TokenRS getCredential(LoginRQ loginRQ, String credential)
    {
        String baseUrl = String.format("%s://%s:%d/", request.getScheme(), request.getServerName(), request.getServerPort());
        RestTemplate restAPi = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(List.of(MediaType.APPLICATION_JSON));
        headers.add("Authorization", credential);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("username", loginRQ.getUsernameEmailPhone());
        body.add("password", loginRQ.getPassword());
        body.add("grant_type", "password");

        HttpEntity<MultiValueMap<String, String>> payload = new HttpEntity<>(body, headers);

        return restAPi.exchange(baseUrl.concat("oauth/token"), HttpMethod.POST, payload, TokenRS.class).getBody();
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Create basic auth
     * -----------------------------------------------------------------------------------------------------------------
     *
     * @Param httpHeaders
     * @Return String
     */
    public String oauth2Credential()
    {
        String credentials = environment.getProperty("security.oauth2.client.clientId") + ":" + environment.getProperty("security.oauth2.client.clientSecret");
        return "Basic " + new String(Base64.encodeBase64(credentials.getBytes()));
    }


    public String basicToken() {
        String credentials = environment.getProperty("security.oauth2.client.clientId") + ":" + environment.getProperty("security.oauth2.client.clientSecret");
        return  "Basic " + new String(Base64.encodeBase64(credentials.getBytes()));
    }

}
